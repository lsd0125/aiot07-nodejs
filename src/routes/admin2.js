const express = require('express');
const router = express.Router();

router.get('/admin2/:action?/:id?', (req, res)=>{
    res.json({
        params: req.params,
        url: req.url,
        baseUrl: req.baseUrl,
        locals: res.locals
    })
});

module.exports = router;