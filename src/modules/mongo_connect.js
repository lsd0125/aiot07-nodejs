const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'test';
let _db;

const client = new MongoClient(url, {useUnifiedTopology:true});

client.connect()
    .then(c=>{
        _db = client.db(dbName);
    })
    .catch(ex=>{
        console.log('mongo error:', ex);
    })

const getDB = ()=>{
    if(!_db) throw new Error('no mongo connection!');
    return _db;
}

module.exports = getDB;





