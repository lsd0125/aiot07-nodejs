const express = require('express');
const {v4: uuidv4} = require('uuid');
// const multer = require('multer');
// const upload = multer({dest:'tmp/'});

const session = require('express-session');

const upload = require(__dirname + '/upload-module');
const db = require(__dirname+'/modules/db_connect2');
const getMongoClient = require(__dirname+'/modules/mongo_connect')

const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'sdfkljsdhlkkl89349',
    cookie: {
        maxAge: 1200000
    }
}));


// 自訂 middleware

app.use((req, res, next)=>{
    res.locals.my = '9999';

    next();
});

app.get('/', (req, res)=>{
    res.render('home', {name: 'Shinder', aa: 123});
});

app.get('/json-sales', (req, res)=>{
    const sales = require(__dirname + '/../data/sales');
    res.render('json-sales', { sales });
    //res.json(sales);
});

app.get('/try-qs', (req, res)=>{
    res.json(req.query);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});

app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);

    // res.json({
    //     body: req.body,
    //     contentType: req.get('Content-Type')
    // });
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    console.log(req.file);
    res.json(req.file);
});


app.get('/try-uuid', (req, res)=>{
    res.json({
        a: uuidv4(),
        b: uuidv4(),
    });
});

app.post('/pending', (req, res)=>{
    // res.send('----');
});

app.post('/add-api', (req, res)=>{
    res.json({
        body: req.body,
        results: req.body.a*1 + req.body.b*1
    })
});
app.post('/add2-api', upload.none(), (req, res)=>{
    res.json({
        body: req.body,
        results: req.body.a*1 + req.body.b*1
    })
});

app.get('/pulse-api', (req, res)=>{
    res.json({
        results: 60 + Math.floor(Math.random()*40)
    })
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/m\/09\d{2}-?\d{3}-?\d{3}/i, (req, res)=>{
    let u = req.url.slice(3);
    u = u.split('-').join('').split('?')[0];
    res.json({ u });
});

// const admin2Router = require(__dirname+'/routes/admin2');
// app.use(admin2Router);
app.use('/mybase',  require(__dirname+'/routes/admin2') );

app.get('/try-session', (req, res)=>{
    req.session.myVar = (req.session.myVar+1) || 1;
    res.json({
       session: req.session,
       myVar: req.session.myVar
    });
});

app.get('/try-db', (req, res)=>{
    db.query('SELECT * FROM users')
        .then(([ results ])=>{
            res.json(results);
        })
});
app.get('/try-db2', async (req, res)=>{
    console.log(new Date());
    res.write(new Date().toLocaleString());
    await db.query('START TRANSACTION');
    for(let i=0; i<10000; i++){
        let r = await db.query("INSERT INTO `trytry`(`uuid`) VALUES ('" + uuidv4() + "')");
    }
    await db.query('COMMIT');
    console.log(new Date());
    res.end(new Date().toLocaleString());
});

app.get('/try-mongo', async (req, res)=>{
    const db = getMongoClient();

    const r = await db.collection('books').findOne();
    console.log(r);
    res.json(r);
});

app.get('/try-mongo2', async (req, res)=>{
    const db = getMongoClient();
    console.log(new Date());
    res.write(new Date().toLocaleString());
    for(let i=0; i<100000; i++){
        let r = await db.collection('trytry').insertOne({uuid: uuidv4()});
    }
    console.log(new Date());
    res.end(new Date().toLocaleString());
});

app.use(express.static('public'));

app.use((req, res)=>{
    res.type('text/plain');
    res.status(404).send('找不到網頁');
});

app.listen(3000, ()=>{
    console.log('server started');
})